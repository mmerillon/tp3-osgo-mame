package modele;

import infrastructure.jaxrs.HyperLien;

import javax.ws.rs.client.Client;
import java.util.List;
import java.util.Optional;

public class RechercheSynchroneSequentielle extends RechercheSynchroneAbstraite {

    private final ImplemNomAlgorithme nomAlgo;

    public RechercheSynchroneSequentielle(String nom) {
        this.nomAlgo = new ImplemNomAlgorithme(nom);
    }

    @Override
    public Optional<HyperLien<Livre>> chercher(Livre l, List<HyperLien<Bibliotheque>> bibliotheques, Client client) {
        for (HyperLien<Bibliotheque> hyp : bibliotheques) {
            Optional<HyperLien<Livre>> essai = this.rechercheSync(hyp, l, client);
            if (essai.isPresent()) {
                return essai;
            }
        }
        return Optional.empty();
    }

    @Override
    public NomAlgorithme nom() {
        return this.nomAlgo;
    }
}
