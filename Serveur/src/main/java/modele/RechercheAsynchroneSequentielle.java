package modele;

import infrastructure.jaxrs.HyperLien;

import javax.ws.rs.client.Client;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class RechercheAsynchroneSequentielle extends RechercheAsynchroneAbstraite {

    private final ImplemNomAlgorithme nomAlgo;

    public RechercheAsynchroneSequentielle(String nom) {
        this.nomAlgo = new ImplemNomAlgorithme(nom);
    }

    @Override
    public Optional<HyperLien<Livre>> chercher(Livre l, List<HyperLien<Bibliotheque>> bibliotheques, Client client) {
        List<Future<Optional<HyperLien<Livre>>>> promesses = new ArrayList<>(bibliotheques.size());

        for (HyperLien<Bibliotheque> hyp : bibliotheques) {
            promesses.add(
                this.rechercheAsync(hyp, l, client)
            );
        }

        for (Future<Optional<HyperLien<Livre>>> p : promesses) {
            try {
                Optional<HyperLien<Livre>> essai = p.get();
                if (essai.isPresent()) {
                    return essai;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return Optional.empty();
    }

    @Override
    public NomAlgorithme nom() {
        return this.nomAlgo;
    }
}
