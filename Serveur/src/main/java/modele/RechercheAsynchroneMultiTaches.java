package modele;

import infrastructure.jaxrs.HyperLien;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.InvocationCallback;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

public class RechercheAsynchroneMultiTaches extends RechercheAsynchroneAbstraite {

    private final ImplemNomAlgorithme nomAlgo;

    public RechercheAsynchroneMultiTaches(String nom) {
        this.nomAlgo = new ImplemNomAlgorithme(nom);
    }


    @Override
    public Optional<HyperLien<Livre>> chercher(Livre l, List<HyperLien<Bibliotheque>> bibliotheques, Client client) {
        CountDownLatch count = new CountDownLatch(bibliotheques.size());
        AtomicReference<Optional<HyperLien<Livre>>> resultAtomicReference = new AtomicReference<>(Optional.empty());

        for (HyperLien<Bibliotheque> hyp : bibliotheques) {
            this.rechercheAsyncAvecRappel(
                hyp,
                l,
                client,
                new InvocationCallback<Optional<HyperLien<Livre>>>() {
                    @Override
                    public void completed(Optional<HyperLien<Livre>> livreHyperLien) {
                        if (livreHyperLien.isPresent()) {
                            resultAtomicReference.set(livreHyperLien);
                        }
                        count.countDown();
                    }

                    @Override
                    public void failed(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }
            );
        }

        try {
            count.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return resultAtomicReference.get();
    }

    @Override   
    public NomAlgorithme nom() {
        return this.nomAlgo;
    }
}
