package modele;

import infrastructure.jaxrs.HyperLien;

import javax.ws.rs.client.Client;
import java.util.List;
import java.util.Optional;

public class RechercheSynchroneStreamParallele extends RechercheSynchroneAbstraite {

    private final ImplemNomAlgorithme nomAlgo;

    public RechercheSynchroneStreamParallele(String nom) {
        this.nomAlgo = new ImplemNomAlgorithme(nom);
    }

    @Override
    public Optional<HyperLien<Livre>> chercher(Livre l, List<HyperLien<Bibliotheque>> bibliotheques, Client client) {
        return bibliotheques.parallelStream() // on lance un flot parallèle
            .map(hyp -> this.rechercheSync(hyp, l, client)) // on applique à chaque élément la fonction de recherche
            .filter(optHyp -> optHyp.isPresent()) // on retire les résultats vides
            .findAny() // on prend n'importe quel résultat restant
            .orElse(Optional.empty()) // extraction avec option vide par défaut
        ;
    }

    @Override
    public NomAlgorithme nom() {
        return this.nomAlgo;
    }
}
