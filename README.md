# TP3 Services

## Informations

### Membres du projet
+ Oscar GLOAGUEN
+ Mathieu MÉRILLON

### À noter
+ Les configurations de IntelliJ (avec Modules, Artifacts, ...) sont pré-intégrées dans le dossier .idea/

## Réponses aux questions

### 3.2.2.2 Couche services - JAX-RS
```java
interface Repertoire {
  @PUT
  @Produces(TYPE_MEDIA)
  @Consumes(TYPE_MEDIA)
  @ReponsesPUTOption
  // Requête (méthode http + url) : PUT http://BIBLIO/bibliotheque ou PUT http://PORTAIL/portail
  // Corps : application/xml -> sous ensemble des attributs de livres
  // Réponses (à spécifier par code) :
  // - 404 : pas de résultat
  // - 200 : application/xml -> liste des livres trouvés
    Optional<HyperLien<Livre>> chercher(Livre l);
  
  @PUT
  @ReponsesPUTOption
  @Path(JAXRS.SOUSCHEMIN_ASYNC)
  @Consumes(JAXRS.TYPE_MEDIA)
  @Produces(JAXRS.TYPE_MEDIA)
  // Requête (méthode http + url) : PUT http://BIBLIO/bibliotheque/async
  // Corps : application/xml -> sous ensemble des attributs de livres
  // Réponses (à spécifier par code) :
  // - 404 : pas de résultat
  // - 200 : application/xml -> liste des livres trouvés
    Future<Optional<HyperLien<Livre>>> chercherAsynchrone(Livre l, @Suspended final AsyncResponse ar);
  
  @GET
  @Path(SOUSCHEMIN_CATALOGUE)
  @Produces(TYPE_MEDIA)
  // Requête (méthode http + url) : GET http://BIBLIO/bibliotheque/catalogue ou GET http://PORTAIL/portail/catalogue
  // Corps : aucun
  // Réponses (à spécifier par code) :
  // - 200 : application/xml -> liste des hyperliens vers les livres
    HyperLiens<Livre> repertorier();
}

interface Archive {
  @Path("{id}")
  @ReponsesGETNullEn404
  // Adresse de la sous-ressource : BIBLIO
  // Requête sur la sous-ressource (méthode http + url) : GET http://BIBLIO/bibliotheque/{id}
  // Corps : aucun
  // Réponses (à spécifier par code) :
  // - 404 : ressource non existante
  // - 200 : application/xml -> détails du livre d'id {id}
  Livre sousRessource(@PathParam("id") IdentifiantLivre id) ;

  @Path("{id}")
  @GET
  @Produces(JAXRS.TYPE_MEDIA)
  @ReponsesGETNullEn404
  // Requête sur la sous-ressource (méthode http + url) : GET http://BIBLIO/bibliotheque/{id}
  // Corps : aucun
  // Réponses (à spécifier par code) :
  // - 404 : ressource non existante
  // - 200 : application/xml -> détails du livre d'id {id}
  Livre getRepresentation(@PathParam("id") IdentifiantLivre id);

  @POST
  @ReponsesPOSTEnCreated
  @Consumes(JAXRS.TYPE_MEDIA)
  @Produces(JAXRS.TYPE_MEDIA)
  // Requête (méthode http + url) : POST http://BIBLIO/bibliotheque/
  // Corps : application/xml -> détails du livre à ajouter
  // Réponses (à spécifier par code) :
  // - 200 : ajout du livre effectué
  HyperLien<Livre> ajouter(Livre l);
}

interface AdminAlgo {
  @PUT
  @Path(JAXRS.SOUSCHEMIN_ALGO_RECHERCHE)
  @Consumes(JAXRS.TYPE_MEDIA)
  // Requête (méthode http + url) : PUT http://PORTAIL/admin/recherche
  // Corps : application/xml -> nom de l'algorithme de recherche a utiliser
  // Réponses (à spécifier par code) :
  // - 200 : changement effectué
    void changerAlgorithmeRecherche(NomAlgorithme algo);
}
```

### 3.2.2.3 Couche données - JAXB

#### Livre
Schéma:
```xml
<livre>
   <titre>{titre}</titre>
</livre>
```

Exemple:
```xml
<livre><titre>L'ingénu</titre></livre>
```

#### NomAlgorithme
Schéma:
```xml
<algo
   nom="{nom}">
</algo>
```

Exemple:
```xml
<algo nom="RechercheSynchroneSequentielle"/>
```

### 3.3 Client

Résultats des tests en moyenne pour **25 requêtes** :

| Algorithme | Synchrone (ms) | Asynchrone (ms) |
|------------|----------------|-----------------|
|Sequentielle|942.37|614.77|
|MultiTaches|296.06|214.28|
|StreamParallele|318.51|202.24|
|StreamRx|255.02|222.07|

#### Explications
À l'issue des tests, on peut observer plusieurs phénomènes :

+ Pour une méthode de communication donnée (synchrone/asynchrone), 
  l'algorithme séquentiel sera d'autant plus long que l'algorithme multitâche
  que la machine sur laquelle tourne le serveur possède de processeurs
  (les résultats ci-dessus proviennent d'un ordinateur quad-core). En effet,
  effectuer des recherches en parallèle permet de diviser le temps total de la recherche.
  Cette "division" du temps de recherche en parallèle a pour limite
  le nombre de processeurs de la machine, ainsi que le temps de communication
  entre les bibliothèques, le portail et le client (dans nos tests, ce temps est négligeable car le tout est exécuté sur la même machine)
  
+ La communication asynchrone est globalement plus efficace que la communication séquentielle,
  avec un écart relatif d'autant plus marqué pour l'algorithme séquentiel.
  Ceci peut s'expliquer par le fait que le portail peut lancer une recherche vers une bibliothèque
  avant même que la précédente recherche ait renvoyé son résultat. La communication asynchrone permet
  ainsi d'économiser le temps d'attente correspondant aux allers-retours des résultats et nouvelles recherches
  (encore plus pour l'algorithme séquentiel, puisqu'il y a encore plus de temps d'attente).
